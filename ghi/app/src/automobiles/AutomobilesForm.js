import React, { useState, useEffect } from "react";

export default function AutomobilesForm() {
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [model, setModel] = useState("");
    const [models, setModels] = useState([]);
    
    const handleColor= (event) => {
        const value = event.target.value;
        setColor(value);
    };

    const handleYear= (event) => {
        const value = event.target.value;
        setYear(value);
    };

    const handleVin = (event) => {
        const value = event.target.value;
        setVin(value);
    };
    
    const handleModel = (event) => {
        const value = event.target.value;
        setModel(value);
    };
    
    const handleSubmit = async (event) => {
        event.preventDefault();
    
        const data = {}

        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model


    
        const autosURL = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            "Content-Type": "application/json",
          },
        };
        const response = await fetch(autosURL, fetchConfig);
        if (response.ok) {

    
          setColor("");
          setYear("");
          setVin("");
          setModel("");
        }
    };
    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        }
      };
    
      useEffect(() => {
        fetchData();
      }, []);


      return (
        <div>
          <div className="my-5 container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Add an automobile to inventory</h1>
                  <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="form-floating mb-3">
                      <input
                        value={color} onChange={handleColor} placeholder="color" required type="text" name="color" id="color" className="form-control"
                      />
                      <label htmlFor="model_color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input
                        value={year} onChange={handleYear} placeholder="year" required type="text" name="year" id="year" className="form-control"
                      />
                      <label htmlFor="model_year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input
                        value={vin} onChange={handleVin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"
                      />
                      <label htmlFor="model_vin">VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                      <select
                        value={model} onChange={handleModel} placeholder="model" required type="text" name="model" id="model" className="form-select">
                      <option value="">Model</option>
                      {models.map((model) => {
                        return(
                            <option key={model.id} value={model.id}>
                                {model.name}
                                </option>
                        );
                    })}
                      </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }