import React, {useEffect, useState} from "react";

function ListAutomobile(){
    const [automobiles, setAutomobile] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setAutomobile(data.autos)
            }
        }
        catch(e){}

    }
    useEffect(()=> {
        fetchData();
    }, []);


    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles?.map(automobile => {
                        return(
                        <tr key={automobile.vin}>
                            <td>{automobile.vin}</td>
                            <td>{automobile.color}</td>
                            <td>{automobile.year}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>{automobile.sold? "yes":"no"}</td>
                        </tr>
                        )})}
                </tbody>
            </table>
        </>
    )
}
export default ListAutomobile;
