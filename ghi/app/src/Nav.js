import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">

          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className ="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/models">Car Models</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/models/create">Create a Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobile">Automobile</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/automobiles/create">Create a Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-manufacturer">Create a Manufacturer</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/salesperson">Salespeople</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/salesperson/create">Add a Salesperson</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/customers">Customers</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/customers/create">Add a Customer</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/sales/">Sales</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link " aria-current="page" to="/sales/create">Add a Sale</NavLink>
            </li>
            <li className ="nav-item">
            <NavLink className="nav-link " aria-current="page" to="/sales/history">Sales History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/">Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-technicians/">Create a Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/">Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-appointments/">Create a Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/service-history/">ServiceHistory</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
