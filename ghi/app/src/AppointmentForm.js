import React, { useState, useEffect} from "react";

function AppointmentForm() {
    const [date, setDate] = useState("");
    const [time, setTime]= useState("")
    const [reason, setReason] = useState("");
    // const [status, setStatus] = useState("");
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [technician, setTechnician] = useState("");
    const [technicians, setTechnicians] = useState([]);

    const handleDateChange = (event) => {
        setDate(event.target.value)
    }

    const handleTimeChange = (event) => {
        setTime(event.target.value)
    }

    const handleReasonChange = (event) => {
        setReason(event.target.value)
    }

    // const handleStatusChange = (event) => {
    //     setStatus(event.target.value)
    // }

    const handleVinChange = (event) => {
        setVin(event.target.value)
    }

    const handleCustomerChange = (event) => {
        setCustomer(event.target.value)
    }

    const handleTechnicianChange = (event) => {
        setTechnician(event.target.value)
    }

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.date_time = `${date}:${time}`
        data.reason = reason
        data.vin = vin
        data.customer = customer
        data.technician = technician

        const Url = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers:{
                "content-type": "application/json"
            },
        }
        const response = await fetch(Url, fetchConfig)
        if (response.ok) {
            setDate("")
            setReason("")
            // setStatus("")
            setVin("")
            setCustomer("")
            setTechnician("")
            setTime("")
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Appointment</h1>
                <form id="create-appointment-form" onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input onChange={handleDateChange} placeholder="DateTime" value={date} required type="date" id="date" className="form-control" name="model" />
                        <label htmlFor="model">Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleTimeChange} placeholder="Time" value={time} required type="time" id="time" className="form-control" name="model" />
                        <label htmlFor="model">Time</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleReasonChange} placeholder="Reason" value={reason} required type="text" id="reason" className="form-control" name="model" />
                        <label htmlFor="model">Reason</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} placeholder="Vin" value={vin} required type="text" id="vin" className="form-control" name="model" />
                        <label htmlFor="model">Vin</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleCustomerChange} placeholder="Customer" value={customer} required type="text" id="customer" className="form-control" name="model" />
                        <label htmlFor="model">Customer</label>
                    </div>
                    <div className="mb-3"></div>
                        <select onChange={handleTechnicianChange} required type="" id="technician" name="technician" className="form-select" value={technician}>
                        <option value="">Choose a technician</option>
                        {technicians.map((technician) => (
                        <option value={technician.id} key={technician.id}>
                            {technician.first_name} {technician.last_name}{technician.id}
                        </option>
                        ))}
                        </select>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}

export default AppointmentForm
