import React, {useState, useEffect } from "react"

export default function ModelsList() {
    const [models, setModels] = useState([]);

    async function LoadModels() {
        const response = await fetch ("http://localhost:8100/api/models/");
        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }
    useEffect(() => {
        LoadModels();
    }, []);
    return (
        <>
        <h1>Models</h1>
        <table className="table table-striped"> 
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
            {models.map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img src={model.picture_url} alt="Car" />
                </td>
              </tr>
            );
          })}
            </tbody>
        </table>
        </>
    )
}
