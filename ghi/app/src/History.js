import React, { useEffect, useState } from 'react';
function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [searchVin, setSearchVin] = useState('');
    const handleVinChange = (event) => {
        setSearchVin(event.target.value)
    }
    const fetchData = async() => {
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
        }

        const automobileUrl = 'http://localhost:8080/api/automobileVOs/'
        const automobileResponse = await fetch(automobileUrl);

        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            setAutomobiles(automobileData.AutomobileVOs);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleVinSearch = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url);
        if (response.ok) {
            setAppointments(updatedAppointments => updatedAppointments.filter(appointment => appointment.vin === searchVin));
        }
        const automobileUrl = 'http://localhost:8080/api/automobileVOs/'
        const automobileResponse = await fetch(automobileUrl);
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            setAutomobiles(automobileData.AutomobileVOs);
        }
    }




    let vins = [];
    {automobiles?.map(automobile => {
        vins.push(automobile.vin)
    })}

    return (
        <>
            <div className="container">
            <h1 className="mb-3 mt-3">Service History</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Vin</th>
                            <th>Vip</th>
                            <th>Date/time</th>
                            <th>Reason</th>
                            <th>Customer</th>
                            <th>Technician</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments?.map((appointment) => {
                            {if (vins.includes(appointment.vin)) {
                                var vip = "Yes"
                            }
                            else {var vip ="No"}}
                            return (
                                <tr key={appointment.vin}>
                                    <td>{appointment.vin}</td>
                                    <td>{vip}</td>
                                    <td>{appointment.date_time}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.technician}</td>
                                    <td>{appointment.status}</td>

                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <form className='col' onSubmit={handleVinSearch}>
                    <input className="form-control" id="searchbar" placeholder="Search by VIN" value={searchVin} onChange={handleVinChange}></input>
                    <button className="btn btn-primary">Search</button>
                </form>
            </div>
        </>
    )
}

export default ServiceHistory
