import React, {useEffect, useState} from "react";

function ListManufacturer(){
    const [manufacturers, setManufacturers] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers)
            }
        }
        catch(e){
        }

    }
    useEffect(()=> {
        fetchData();
    }, []);


    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers?.map(manufacturer => (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                        ))}
                </tbody>
            </table>
        </>
    )
}
export default ListManufacturer;
