import React, {useEffect, useState} from "react";


function ListTechnician(){
    const [technician, setTechnician] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                setTechnician(data.technicians)
            }
        }
        catch(e){

        }

    }
    useEffect(()=> {
        fetchData();
    }, []);

    const remove = async (technician) => {
        try {
            const deleteId = technician.id
            const url = `http://localhost:8080/api/technicians/${deleteId}`
            const fetchConfig = {
                method:"delete"
            };
            const response = await fetch(url, fetchConfig);
            if (response.ok){
                fetchData()
            }
        }
        catch(e){

        }
    };

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee id</th>
                        <th>Id</th>
                    </tr>
                </thead>
                <tbody>
                    {technician?.map(technicians => {
                        return(
                        <tr key={technicians.id}>
                            <td>{technicians.first_name}</td>
                            <td>{technicians.last_name}</td>
                            <td>{technicians.employee_id}</td>
                            <td>{technicians.id}</td>
                            <td><button onClick={()=> remove(technicians)}>Delete</button></td>
                        </tr>
                        )})}
                </tbody>
            </table>
        </>
    )
}
export default ListTechnician;
