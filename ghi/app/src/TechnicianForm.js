import React, { useState, useEffect} from "react";

function TechnicianForm() {
    const [first_name, setFirstName] = useState("");
    const [last_name, setLastName] = useState("");
    const [employee_id, setEmployeeId] = useState("");
    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value)
    }
    const handleLastNameChange = (event) => {
        setLastName(event.target.value)
    }
    const handleEmployeeIdChange = (event) => {
        setEmployeeId(event.target.value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name
        data.last_name = last_name
        data.employee_id = employee_id
        const url = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers:{
                "content-type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFirstName("")
            setLastName("")
            setEmployeeId("")

        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Technician</h1>
                <form id="create-technician-form" onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} placeholder="Technician" value={first_name} required type="text" id="technician" className="form-control" name="model" />
                        <label htmlFor="model">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} placeholder="Technician" value={last_name} required type="text" id="technician" className="form-control" name="model" />
                        <label htmlFor="model">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIdChange} placeholder="Technician" value={employee_id} required type="text" id="technician" className="form-control" name="model" />
                        <label htmlFor="model">Employee id</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}

export default TechnicianForm
