import React, { useState } from "react";

export default function SalesPersonForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeNumber, setEmployeeNumber] = useState("");
  const [hired, setHired] = useState(false);
  const [notHired, setNotHired] = useState(false);


  const handleFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeNumber = (event) => {
    const value = event.target.value;
    setEmployeeNumber(value);
  };

  const data = {
    first_name: firstName,
    last_name: lastName,
    employee_number: employeeNumber,
  };

  const salespersonURL = "http://localhost:8090/api/salespeople/";
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(salespersonURL, fetchConfig);
    if (response.ok) {
      await response.json();
      setFirstName("");
      setLastName("");
      setEmployeeNumber("");
      setHired(true);
    } else if (response.status !== 200) {
      setNotHired(true);
    }
  };

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  let messageFailedClasses = 'alert alert-danger d-none mb-0'
  let anotherForm = 'btn btn-primary d-none'
  if (hired) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none'
    anotherForm = 'btn btn-primary'
  } else if (notHired) {
    messageFailedClasses = 'alert alert-danger mb-0'
  }

  return (
    <div>
      <div className="my-5 container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Salesperson</h1>
              <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                  <input
                    value={firstName} onChange={handleFirstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control"
                  />
                  <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={lastName} onChange={handleLastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control"
                  />
                  <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={employeeNumber} onChange={handleEmployeeNumber} placeholder="Employee Number" required type="text" name="employee_number" id="employee_number" className="form-control"
                  />
                  <label htmlFor="employee_number">Employee Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
                <div className={messageClasses} id="success-message">
                  Successfully Created A New Sales Person!
                </div>
                <div className={messageFailedClasses} id="unsuccessful-message">
                  Unsuccessful creation, make sure employee ID is unique.
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}