import React, { useState, useEffect} from "react";

export default function ManufacturerForm() {
    const [manufacturer, setManufacturers] = useState("");
    const handleManufacturerChange = (event) => {
        setManufacturers(event.target.value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = manufacturer
        const url = "http://localhost:8100/api/manufacturers/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers:{
                "content-type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setManufacturers("")

        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Manufacturer</h1>
                <form id="create-manufacturer-form" onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input onChange={handleManufacturerChange} placeholder="Manufacturer" value={manufacturer} required type="text" id="manufacturer" className="form-control" name="model" />
                        <label htmlFor="model">Manufacturer</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}
