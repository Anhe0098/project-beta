import React, {useEffect, useState} from "react";


function ListAppointment(){
    const [appointment, setAppointment] = useState([]);
    const [automobile, setAutomobile] = useState([]);

    const handleFinish = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish`, {"method": "PUT"})
        if (response.ok){
            setAppointment(newAppointment => newAppointment.filter(appointment => appointment.id !== id))
        }
    }
    const handleCancel = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel`, {"method": "PUT"})
        if (response.ok){
            setAppointment(newAppointment => newAppointment.filter(appointment => appointment.id !== id))
        }
    }
    function edit(appointment) {
        if(appointment.status === "Cancel" || appointment.status === "Finish") {
            return true
        }
        return false
    }

    const fetchData = async() => {
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setAppointment(data.appointments);
        }
        const automobileUrl = 'http://localhost:8080/api/automobileVOs/'
        const automobileResponse = await fetch(automobileUrl);

        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            setAutomobile(automobileData.AutomobileVOs);
        }
    }
        useEffect(()=> {

            fetchData();
        }, []);
    let vins = [];
    {automobile?.map(automobiles => {
        vins.push(automobiles.vin)
    })}

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Vip</th>
                        <th>Date/Time</th>
                        <th>Reason</th>
                        <th>Customer</th>
                        <th>Technician</th>
                    </tr>
                </thead>
                <tbody>
                {appointment?.map((appointments) => {
                    {if (!edit(appointments)) {
                        {if (vins.includes(appointments.vin)) {
                            var vip = "Yes"
                        }
                        else {var vip ="No"}}
                            return (
                                <tr key={appointments.vin}>
                                    <td>{appointments.vin}</td>
                                    <td>{vip}</td>
                                    <td>{appointments.date_time}</td>
                                    <td>{appointments.reason}</td>
                                    <td>{appointments.customer}</td>
                                    <td>{appointments.technician}</td>
                                    <td>
                                        <button onClick={() => handleCancel(appointments.id)}>Cancel</button>
                                    </td>
                                    <td>
                                        <button onClick={() => handleFinish(appointments.id)}>Finish</button>
                                    </td>
                                </tr>
                            )
                        }}})}
                    </tbody>
            </table>
        </>
    )
}
export default ListAppointment;
