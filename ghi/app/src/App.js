import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ModelsList from './models/ModelsList';
import ModelsForm from './models/ModelsForm';
import AutomobilesForm from './automobiles/AutomobilesForm';
import SalesPersonList from './salesperson/SalesPersonList';
import SalesPersonForm from './salesperson/SalesPersonForm';
import CustomersList from './customers/CustomersList';
import CustomersForms from './customers/CustomersForms';
import SalesList from './sales/SalesList';
import SalesForm from './sales/SalesForm';
import SalesHistory from './sales/SalesHistory';
import ListManufacturer from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ListAutomobile from './ListAutomobile';
import ListTechnician from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import ListAppointment from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './History';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="models">
            <Route index element={<ModelsList />} />
              <Route path="create" element={<ModelsForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesForm/>} />
              <Route path="create" element={<AutomobilesForm />} />
          </Route>
          <Route path="salesperson">
            <Route index element={<SalesPersonList/>} />
              <Route path="create" element={<SalesPersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomersList/>} />
              <Route path="create" element={<CustomersForms />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList/>} />
              <Route path="create" element={<SalesForm />} />
              <Route path="history" element={<SalesHistory />} />
          </Route>
          <Route path="/manufacturers" element={<ListManufacturer />} />
          <Route path="/create-manufacturer" element={<ManufacturerForm />} />
          <Route path="/automobile" element={<ListAutomobile />} />
          <Route path="/technicians" element={<ListTechnician />} />
          <Route path="/create-technicians" element={<TechnicianForm />} />
          <Route path="/appointments" element={<ListAppointment />} />
          <Route path="/create-appointments" element={<AppointmentForm />} />
          <Route path="/service-history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
