# CarCar

Team:

* Anthony He - Service
* Marvin Xia - Sales

CarCar is an automobile dealership service website that caters to both its customers and employees. A user is able to utilize site and access records regarding available stock, view sales records, or even schedule a service appointment for a vehicle.

## Design
<img src="Diagram.PNG"
     alt="Diagram image"
     style="width: 700px;" />

## Getting Started
1. Fork and clone https://gitlab.com/Anhe0098/project-beta.git into desired project directory via terminal
2. Once cloned a new directory will be created named "project-beta"
3. Navigate/CD into newly created "project-beta" directory
4. Run docker application as this project relies on its to ensure an isolated work environment
5. in the command terminal create a new volume via the command:  ```docker volume create "beta-data"```
6. Next the docker images will need to be built by running the following command within your terminal ```docker compose build```
7. Finally to build the containers and run the applicaiton itself run the command ```docker compose up```
8. If everything was done properly there will be a total of 7 containers up and running along with the terminal window displaying a message stating that the server had been complied successfully
9. If the projectis running sucessfully the web application can be run through http://localhost:3000 in your browser (Google Chrome preferrably)
10. Otherwise the use of CRUD operations in the database via API can be done as well with the following instructions of the inventory, sales, service microservices:


## Inventory  microservice
The inventory microservice contains three specific models to represent the stock and inventory of the automobile services: the Manfacturer, vehicle, and automobile.

The Manufacturer model uses only one spefici field to represent a car companty with the use of a characterfield value.

The vehicle model which has three speficic properties of name, picture URL and manufacturer(which is a foreign key of the manufacturer name). These three as listed are to represent a vehicle that is in stock

Lastly the automobile model with the fields color, year, vin, smodel and sold. This model in particular is the representation of a single car in inventory and determins whether or not it is in stock or has been sold already. It determines it own unique value through the use of the unique field for the VIN field. It additionally holds a foreign key relationship with the model field.

### Manufacturer (Port: 8100)

- List/Create: http://localhost:8100/api/manufacturers/
- Get Details/Update/Delete: http://localhost:8100/api/manufacturers/:id/

#### Sample GET Response (List Manufacturers)

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

#### Sample POST (Create manufacturer) and PUT (Update manufacturer) Request body

Request shape is the same for both, only manufacturer name is required

```
{
  "name": "Chrysler"
}
```

#### Sample Response for GET (Get specific manufacturer details), PUT (Update manufacturer), and DELETE (Delete manufacturer)

```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```

### Vehicle models

- List/Create: http://localhost:8100/api/models/
- Get Details/Update/Delete: http://localhost:8100/api/models/:id/

#### Sample GET Response (List vehicle models)

```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

#### Sample POST request body (Create vehicle model)

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

#### Sample PUT request body (Update vehicle model)

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```

#### Sample GET (Get specific vehicle model details), POST (Create vehicle model), and PUT (Update vehicle model) Response

```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```

### Automobiles

- List/Create: http://localhost:8100/api/automobiles/
- Get Details/Update/Delete: http://localhost:8100/api/automobiles/:vin/

#### Sample GET Response (List automobiles)

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}
```

#### Sample POST request body (Create automobile)

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```

#### Sample GET response (Get specific automobile details)

```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```

#### Sample PUT Request body + Response (Update automobile)

Request body and Response have same shape

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```



## Service microservice

The Service microservice works with the service portion of the dealership. Here an individual can schedule an appointment for car repairs or work. This microservice focuses specifically on four specific models to work: The technician model to create an employee for the service, an appointment model to set a date and time, and lastly an automobileVO that represents a vehicle that needs to be brough in for work.
This microservice works with Django frame work in the backend and relies on models to provide information to our views page to create information. The views function then get used by urls to create a pathway to create working api.

Service Microservice has three models that are important to the backend function
- Technician: refers to the technicians employees and have the fields: first_name, last_name, employee_id
- AutomobileVO: a value objects that uses a poller to communication with the inventory side to get the vehicle vins and wether or not its sold.
- Appointment: refers to being able to create and manage appointments and have the fields:
date_time, reason, status, vin, customer, and a technician foreign key.

These models work in tandem with the views and urls to create functional api backend.

API: api_list_technicians - Lists and shows the detail of the technicians
api_detail_technicians- Create the technicians
api_list_appointment- List and shows the appointments
api_detail_appointment- Creates the appointments
api_get_automobileVO- gets the vin and sold.
api_finish_appointment - Deals with the finish appointment button
api_cancel_appointment - Deals with the cancel appointment button

## Sales microservice

The sales microservice utilizes and deals with information regarding all administrative work related to sales and record keeping of the dealership. This microservice relies heavily on four specifc model classes: Salesperson, Cuistomer,  AutomobileVO, and Sale that will act as the backbone in which all other class and functions will draw on to perform their given tasks.

These four models have their own specific features that are improtnant to their own individual functions and are as follows:
- Salesperson: refers to a salesperson of the carcar service that contains the fields: first_name, last_name, and employee_number
- Customer: refers to a customer of the carcar service that contains the fields: first_name, last_name, phone_number, and address
- AutomobileVO: a value object that uses a poller to communicate with the inventory microservice to get a vehicle's VINs
- Sale - refers to the sale of an automobile with the field price with foriegn keys automobile, customer ,and salesperson

As with any Django framework these models are referenced and work in conjunction with the views.py functions and the urls.py in order to properly create a functional API in the backend database.

In the views the following functions work as follows:
- api_list_salesperson() works to obtain a list of salespeople within the carcar service as well as create a new employee or delete one in case one is no longer with the service
- api_list_customers() list, creates, and deletes any customers with the carcar
- api_list_sales() that creates, list, and deletes all sales and references the three relationships of the foreign keys: automobile, customer, and salesperson

**Salesperson API**

| Action | Method | URL |
| ----------- | ----------- |  ----------- |
| List salespeople	 | GET | http://localhost:8090/api/salespeople/
 Create a salespeople	|POST |http://localhost:8090/api/salespeople/
| Delete a specific salespeople	 | DELETE | http://localhost:8090/api/salespeople/:id


To get a list of salespeople in the GET response the following example json body can be used to create a new salesperson in POST
```
{
  "first_name": "Test First Name",
  "last_name": "Test Last Name",
  "employee_number": 12345,
	"id": 1
}
```
which will result in the list below:
```
{
	"salespeople": [
		{
			"first_name": "Test First Name",
			"last_name": "Test Last Name",
			"employee_number": "12345",
			"id": 1
		}
	]
}
```

**Customer API**

| Action | Method | URL |
| ----------- | ----------- |  ----------- |
| List customer		 | GET | 	http://localhost:8090/api/customers/
 Create a customer	|POST |http://localhost:8090/api/customers/
| Delete a specific customer	 | DELETE | http://localhost:8090/api/customers/:id

To get a list of customers in the GET response the following example json body can be used to create a new salesperson in POST
```
{
  "first_name": "First ",
  "last_name": "Last",
  "phone_number": "123-456-7890",
	"address": "test address",
}
```
which will result in the list below:
```
{
	"customers": [
		{
			"first_name": "First ",
			"last_name": "Last",
			"address": "test address",
			"phone_number": "123-456-7890",
			"id": 1
		}
	]
}
```

**Sales API**

| Action | Method | URL |
| ----------- | ----------- |  ----------- |
| List sales			 | GET | http://localhost:8090/api/sales/
 Create a sale	|POST |http://localhost:8090/api/sales/
| Delete a sale	 | DELETE | http://localhost:8090/api/sales/:id

To get a list of customers in the GET response the following example json body can be used to create a new salesperson in POST
```
{
    "salesperson": "Test First",
    "customer": Test,
    "automobile": "1C3CC5FBN1223175",
    "price": 1000
}
```
which will result in the list below:
```
{
	"automobile": {
		"vin": "1C3CC5FBN1223175",
		"id": 1
	},
	"salesperson": {
		"first_name": "Test First Name",
		"last_name": "Test Last Name",
		"employee_number": "12345",
		"id": 1
	},
	"customer": {
		"first_name": "First ",
		"last_name": "Last",
		"address": "test address",
		"phone_number": "123-456-7890",
		"id": 1
	},
	"price": 1000,
	"id": 1
}
```

**Technicans**

#### GET Response (List technicians)
http://localhost:8080/api/technicians/
```
{
    "technicians": [
        {
        "first_name": "Hi",
        "last_name": "There",
        "employee_id": "3"
        },
    ]
}

#### POST Response (Create technicians)
http://localhost:8080/api/technicians/
```
{
    "technicians": [
        {
        "first_name": "Hi",
        "last_name": "There",
        "employee_id": "3"
        id: 1
        },
    ]
}

#### DELETE Response (Delete technician)
Request and response JSON will have the same shape
```
{
            "first_name": "Hi",
            "last_name": "There",
            "employee_id": "3",
            "id": 6
        },
```
#### Appointments - MIGHT HAVE TO MAKE APPOINTMENTS THROUGH THE WEBSITE.
http://localhost:8080/api/appointments/
**List Appointments**
```
		{
			"date_time": "2023-07-29T13:39:00+00:00",
			"reason": "testsetsetset",
			"status": "",
			"vin": "setsetsetsetsetsetesset",
			"customer": "tsetsetsetset",
			"technician": "Pls",
			"id": 6
		}
```
**Create Appointment**
http://localhost:8080/api/appointments/
```
		{
			"date_time": "2023-07-29T13:39:00+00:00",
			"reason": "testsetsetset",
			"status": "",
			"vin": "setsetsetsetsetsetesset",
			"customer": "tsetsetsetset",
			"technician": 1
		}
```
http://localhost:8080/api/appointments/id/
**Delete Appointments**
```
{
    "appointments": [
        {
            "id": 1,
            "date_time": "2023-06-23T12:00:00+00:00",
            "reason": "test",
            "status": "test",
            "vin": "test",
            "customer": "test",
            "technician_id": 1
        }
    ]
}
```
**Update Finish Appointments**
http://localhost:8080/api/appointments/1/finish
```
{
	"appointment": {
		"date_time": "2023-06-03T21:52:00+00:00",
		"reason": "test",
		"status": "",
		"vin": "test",
		"customer": "test",
		"technician": "Pls",
		"id": 1
	}
}
```
can use ID to update appointment

**Update Cancel Appointments**
http://localhost:8080/api/appointments/1/cancel
```
{
	"appointment": {
		"date_time": "2023-06-03T21:52:00+00:00",
		"reason": "test",
		"status": "",
		"vin": "test",
		"customer": "test",
		"technician": "Pls",
		"id": 1
	}
}
```
can use ID to update appointment
