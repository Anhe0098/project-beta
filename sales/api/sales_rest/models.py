from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=True)
    import_href = models.CharField(max_length=200, unique=True,)


class SalesPerson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()


class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20)
    

class Sales(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO, 
        related_name="sales",
        on_delete=models.CASCADE,
    )
    
    salesperson = models.ForeignKey(
        SalesPerson, 
        related_name="sales",
        on_delete=models.CASCADE,
    )    

    customer = models.ForeignKey(
        Customer, 
        related_name="sales",
        on_delete=models.CASCADE,
    )        
    
    price = models.PositiveBigIntegerField()