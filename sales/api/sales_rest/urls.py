from django.urls import path 
from .views import salesperson, salesperson_detail, customer, customer_detail, sale, sale_detail

urlpatterns = [
    path("salespeople/", salesperson, name="salesperson"),
    path("salespeople/<int:id>/", salesperson_detail, name="salesperson_detail"),
    path("customers/", customer, name="customer"),
    path("customers/<int:id>/", customer_detail, name="customer_detail"),
    path("sales/", sale, name="sale"),
    path("sales/<int:id>/", sale_detail, name="sale_detail"),
    ] 