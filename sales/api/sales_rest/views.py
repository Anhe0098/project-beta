from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import SalesPerson, Customer, Sales, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id"
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "first_name",
        "last_name",
        "employee_number",
        "id"
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id"
    ]


class SalesEncoder(ModelEncoder):
    model = Sales
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id"
    ]    
    
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def salesperson(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse({"salespeople": salespeople}, encoder=SalesPersonEncoder)
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(salesperson, encoder=SalesPersonEncoder, safe=False)
        except:
            return JsonResponse({"message": "More info needed"}, status=400)
        

@require_http_methods(["GET","DELETE"])
def salesperson_detail(request, id):
    try:
        count,_=SalesPerson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    except SalesPerson.DoesNotExist:
        response = JsonResponse({"message":"Sales Person does not exist"})
        return response


@require_http_methods(["GET", "POST"])
def customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse({"customer": customer}, encoder=CustomerEncoder)
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(customers, encoder=CustomerEncoder, safe=False)
        except:
            return JsonResponse({"message": "More info needed"}, status=400)
        
@require_http_methods(["GET","DELETE"])
def customer_detail(request):
    try:
        count,_=Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    except Customer.DoesNotExist:
        response = JsonResponse({"message":"Customer does not exist"})
        return response
    
    
@require_http_methods(["GET", "POST"])
def sale(request):
    if request.method == "GET":
        sales = Sales.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            salesperson_id = content["salesperson"]
            salesperson = SalesPerson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson could not be found"},
                status=400
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer could not be found"},
                status=400
            )
        try:
            auto_id = content['automobile']
            automobile = AutomobileVO.objects.get(id=auto_id)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile could not be found"},
                status=400
            )

        sale = Sales.objects.create(**content)
        return JsonResponse(sale, encoder=SalesEncoder, safe=False)
    
@require_http_methods(["GET", "DELETE"])
def sale_detail(request, id=None):
    if request.method == "DELETE": 
        try: 
            count, _ = Sales.objects.filter(id=id).delete()
            if count == 0:
                response = JsonResponse({"message": "Sale not found"})
                response.status_code = 404
                return response
            return JsonResponse ({"deleted": count > 0})
        except: 
            response = JsonResponse({"message": "Error deleting sale"})
            response.status_code = 400
            return response

