from django.shortcuts import render
from common.json import ModelEncoder, DateEncoder
from django.http import JsonResponse
from .models import Technician, AutomobileVO, Appointment
from django.views.decorators.http import require_http_methods
import json
from django.core import serializers
from datetime import datetime
# Create your views here.

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"

    ]

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id",
    ]
    encoder = {
        "technician": TechnicianDetailEncoder,
        "date_time": DateEncoder
    }
    def get_extra_data(self, o):
        return {"technician": o.technician.first_name,
                "date_time": o.date_time
                }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id"
    ]
    encoder = {
        "technician": TechnicianDetailEncoder,
                "date_time": DateEncoder
    }
    def get_extra_data(self, o):
        return {"technician": o.technician.first_name,
                "date_time": o.date_time
                }



@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians}, encoder=TechnicianListEncoder)
    else:
        try:
            content = json.loads(request.body)
            technicians = Technician.objects.create(**content)
            return JsonResponse(technicians, encoder=TechnicianListEncoder, safe=False)
        except:
            return JsonResponse({"message": "Invalid Input"}, status=400)

@require_http_methods(["GET", "DELETE"])
def api_detail_technicians(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(technician, encoder=TechnicianDetailEncoder)
    else:
        try:
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            return response



@require_http_methods(["GET", "POST"])
def api_list_appointment(request,):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments": appointments}, encoder=AppointmentListEncoder)
    else:
        try:
            content = json.loads(request.body)
            content['date_time'] = datetime.fromisoformat(content['date_time'])
            technician_id = content.pop('technician')
            technician = Technician.objects.get(id=technician_id)
            appointment = Appointment.objects.update_or_create(technician=technician, **content)
            return JsonResponse(appointment,encoder=AppointmentDetailEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid Input"}, status=400)

@require_http_methods(["GET", "DELETE"])
def api_detail_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse({"appointment":appointment}, encoder=AppointmentDetailEncoder,safe=False)
    else:
        if request.method == "DELETE":
            try:
                count, _ = Appointment.objects.filter(id=pk).delete()
                return JsonResponse({"deleted": count > 0})
            except Appointment.DoesNotExist:
                response = JsonResponse({"message": "Technician does not exist"})
                return response

@require_http_methods(["GET"])
def api_get_automobileVO(request):
    if request.method == "GET":
        automobileVO = AutomobileVO.objects.all()
        return JsonResponse({"automobileVO": automobileVO}, encoder=AutomobileVOEncoder, safe=False)

@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=pk)
        appointment.Finish()
        return JsonResponse({"appointment":appointment}, encoder=AppointmentListEncoder,safe=False)

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=pk)
        appointment.Cancel()
        return JsonResponse({"appointment":appointment}, encoder=AppointmentListEncoder,safe=False)
