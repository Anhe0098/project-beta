from django.db import models

# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, unique=True)
    sold = models.CharField(max_length=100)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=200, unique=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )
    def Finish(self):
        self.status = "finished"
        self.save()
    def Cancel(self):
        self.status = "canceled"
        self.save()

    def __str__(self):
        return f'{self.date_time}'
