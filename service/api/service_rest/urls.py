from django.urls import path, include
from .views import api_list_technicians, api_detail_technicians, api_list_appointment, api_get_automobileVO, api_detail_appointment, api_finish_appointment, api_cancel_appointment

urlpatterns = [
    path("technicians/", api_list_technicians, name="technicians"),
    path("technicians/<int:pk>/", api_detail_technicians, name="technicians_detail"),
    path("appointments/", api_list_appointment, name="appointments"),
    path("appointments/<int:pk>/", api_detail_appointment, name="api_show_appointment"),
    path("appointments/<int:pk>/finish", api_finish_appointment, name="api_finish_appointment"),
    path("appointments/<int:pk>/cancel", api_cancel_appointment, name="api_cancel_appointment"),
    path("automobileVOs/", api_get_automobileVO, name="automobileVOs"),

]
